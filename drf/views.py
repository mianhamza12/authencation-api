from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from .serialization import stuserializer, loginserialization, profileserialization,passwordchangesri,sendpasswrodemailsri,emailchangepaswordsir
from django.contrib.auth import authenticate
from .renders import userrenderers
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.permissions import IsAuthenticated
from .models import User


# Create your views here.
# generate token manually
def get_tokens_for_user(user):
    refresh = RefreshToken.for_user(user)
    return {
        'refresh': str(refresh),
        'access': str(refresh.access_token),
    }


class registraion(APIView):
    renderer_classes = [userrenderers]
    def post(self, request):
        serializer = stuserializer(data=request.data)
        if serializer.is_valid():
            fb = serializer.save()
            token = get_tokens_for_user(fb)
            return Response({'token':token,'create': 'user create!!!'}, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#
class login_fu(APIView):
    renderer_classes = [userrenderers]

    def post(self, request):
        fm = loginserialization(data=request.data)
        if fm.is_valid():
            email = fm.data.get('email')
            password = fm.data.get('password')
            user = authenticate(email=email, password=password)
            if user is not None:
                token = get_tokens_for_user(user)
                return Response({'token':token,'login': 'userlogin success!!!'}, status=status.HTTP_200_OK)
        else:
            return Response({'errors': {'non_field_errors': ['email and password is not valid']}},
                            status=status.HTTP_400_BAD_REQUEST)


class profile_fu(APIView):
    renderer_classes = [userrenderers]
    permission_classes = [IsAuthenticated]
    def get(self, request, format=None):
        fm = profileserialization(request.user)
        return Response(fm.data, status=status.HTTP_200_OK)
        # else:
        #     return Response(fm.errors,status=status.HTTP_400_BAD_REQUEST)

class passwordchange_fu(APIView):
    renderer_classes = [userrenderers]
    permission_classes = [IsAuthenticated]
    def post(self,request,format=None):
        fm=passwordchangesri(data=request.data,context={'user':request.user})
        if fm.is_valid():
            return Response({"password":"password change"},status=status.HTTP_200_OK)
        else:
            return Response(fm.errors,status=status.HTTP_400_BAD_REQUEST)

class sendpasswordemail_fu(APIView):
    renderer_classes = [userrenderers]
    def post(self,request):
        fm=sendpasswrodemailsri(data=request.data)
        if fm.is_valid():
            return Response({'Email':'email send to your mail adress kindly check '},status=status.HTTP_200_OK)
        else:
            return Response(fm.errors,status=status.HTTP_400_BAD_REQUEST)

class emailpasswordchange_fu(APIView):
    renderer_classes = [userrenderers]
    def post(self,request,uid,token):
        fm=emailchangepaswordsir(data=request.data,context={'uid':uid, 'token':token})
        if fm.is_valid():
            return Response({'Email change': 'password change '}, status=status.HTTP_200_OK)
        else:
            return Response(fm.errors, status=status.HTTP_400_BAD_REQUEST)

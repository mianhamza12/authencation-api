from django.contrib import admin
from .models import User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
# Register your models here.
class userModelAdmin(BaseUserAdmin):

    list_display = ["id","email", "username","tc", "is_admin"]
    list_filter = ["is_admin"]
    fieldsets = [
        ('user credentials', {"fields": ["email", "password"]}),
        ("Personal info", {"fields": ["username","tc"]}),
        ("Permissions", {"fields": ["is_admin"]}),
    ]
    add_fieldsets = [
        (
            None,
            {
                "classes": ["wide"],
                "fields": ["email", "username","tc", "password1", "password2"],
            },
        ),
    ]
    search_fields = ["email"]
    ordering = ["email","id"]
    filter_horizontal = []


admin.site.register(User, userModelAdmin)

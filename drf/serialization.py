from rest_framework import serializers
from .models import User
from django.contrib.auth.hashers import make_password
from django.utils.encoding import smart_str,force_bytes,DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_decode,urlsafe_base64_encode
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from .utils import utli


class stuserializer(serializers.ModelSerializer):
    # password2 = serializers.CharField(style={'input_type': 'password'}, write_only=True)
    class Meta:
        model = User
        fields = ['email', 'username', 'password', 'tc']
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def create(self, validate_data):
        username = validate_data['username']
        email = validate_data['email']
        password = validate_data['password']
        user = User.objects.create(username=username, email=email, password=make_password(password))
        user.save()
        return user


class loginserialization(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=255)

    class Meta:
        Model = User
        fields = ['email', 'password']


class loginserialization(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=255)

    class Meta:
        model = User
        fields = ['email', 'password']


class profileserialization(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','email', 'username','tc']

class passwordchangesri(serializers.Serializer):
    password=serializers.CharField(max_length=225,style={'input_type':'password'},write_only=True)
    password2 = serializers.CharField(max_length=225, style={'input_type': 'password'}, write_only=True)
    class Meta:
        fields=['password','password2']

    def validate(self, attrs):
        pas=attrs.get('password')
        pas2=attrs.get('password2')
        user=self.context.get('user')
        if pas!=pas2:
            raise serializers.ValidationError("password not match ")

        user.set_password(pas)
        user.save()
        return attrs

class sendpasswrodemailsri(serializers.Serializer):
    email=serializers.EmailField(max_length=255)
    class Meta:
        fields=['email']

    def validate(self, attrs):
        email=attrs.get('email')
        if User.objects.filter(email=email).exists():
            user=User.objects.get(email=email)
            uid=urlsafe_base64_encode(force_bytes(user.id))
            print(uid)
            token=PasswordResetTokenGenerator().make_token(user)
            link="http//:localhost:8080/emailpassword/"+uid+"/"+token
            print("Password link:",link)
            body="click the link to rest password"+link
            data={
                'subject':'rest password',
                'body':body,
                'to_email':user.email
            }
            utli.send_email(data)

            return attrs
        else:
            raise serializers.ValidationError("Email not found")


#this function for emai send password
class emailchangepaswordsir(serializers.Serializer):
    password = serializers.CharField(max_length=225, style={'input_type': 'password'}, write_only=True)
    password2 = serializers.CharField(max_length=225, style={'input_type': 'password'}, write_only=True)

    class Meta:
        fields = ['password', 'password2']


    def validate(self, attrs):
        pas = attrs.get('password')
        pas2 = attrs.get('password2')
        print('1 password')
        uid=self.context.get('uid')
        token=self.context.get('token')
        if pas != pas2:
            raise serializers.ValidationError("password not match ")
        id=smart_str(urlsafe_base64_decode(uid))
        print("id",id)
        user=User.objects.get(id=id)
        print("user",user)
        if not PasswordResetTokenGenerator().check_token(user,token):
            raise serializers.ValidationError("token fail ")
        user.set_password(pas)
        user.save()
        return attrs


